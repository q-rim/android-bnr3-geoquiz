package com.q_rim.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CheatActivity extends Activity {

  public static final String KEY_EXTRA = "com.q-rim.android.geoquiz.answer";      // key for answer - passed fr:  Activity_1 --> Activity_2
  public static final String KEY_EXTRA_DID_USER_CHEAT = "com.q-rim.android.geoquiz.did_user_cheat";   // key for boolean:  did user cheat? - passed fr:  Activity_1 --> Activity_2
  final private String TAG = "---Q.c---";

  private TextView answerTextView;
  private Button showAnswerButton;
  private Boolean answer, didUserCheat;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cheat);

    // data fr:  Activity_2 --> Activity_1
    didUserCheat = false;
    sendDataToQuizActivity(didUserCheat);

    answer = getIntent().getBooleanExtra(KEY_EXTRA, false);     // data fr:  Activity_1 --> Activity_2

    showAnswerButton = (Button)findViewById(R.id.showAnswer_Button);
    showAnswerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view){
        Log.d(TAG, "showAnswerButton onClick(): ");

        // data fr:  Activity_2 --> Activity_1
        didUserCheat = true;
        sendDataToQuizActivity(didUserCheat);

        answerTextView = (TextView)findViewById(R.id.answer_TextView);
        answerTextView.setText("Answer is:  "+answer);

        Toast.makeText(getApplicationContext(), "You are a CHEATER!!!", Toast.LENGTH_SHORT).show();
      }
    });
  }

  private void sendDataToQuizActivity(boolean userCheated) {
    Intent dataA2_to_A1 = new Intent();
    dataA2_to_A1.putExtra(KEY_EXTRA_DID_USER_CHEAT, userCheated);
    setResult(RESULT_OK, dataA2_to_A1);
  }
}
