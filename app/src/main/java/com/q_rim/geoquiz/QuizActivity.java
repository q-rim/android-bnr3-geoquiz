package com.q_rim.geoquiz;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

  private static final String TAG = "-----Q-----";
  private Button trueButton, falseButton, nextButton, previousButton, cheatButton;
  private TextView questionTextView, answerTextView, apiLevelTextView;
  private boolean answer, didUserCheat;

  private int currIndex = 0;
  private TrueFalse[] questionBank = new TrueFalse[] {
    new TrueFalse(R.string.question_oceans, true),
    new TrueFalse(R.string.question_mideast, false),
    new TrueFalse(R.string.question_africa, false),
    new TrueFalse(R.string.question_americas, true),
    new TrueFalse(R.string.question_asia, true),
  };

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    // Getting back data from Activity_2 --> Activity_1
    if (data == null) {
      return;
    }
    // data fr:  Activity_2 --> Activity_1
    didUserCheat = data.getBooleanExtra(CheatActivity.KEY_EXTRA_DID_USER_CHEAT, false);
    Log.d(TAG, "didUserCheat: " +didUserCheat);   Toast.makeText(getApplicationContext(), "didUserCheat: " + didUserCheat, Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "onCreate() started");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_quiz);

    setQuestionTextView(currIndex);

    // displaying Android API Level
    String apiLevel = ""+Build.VERSION.SDK_INT;
    apiLevelTextView = (TextView)findViewById(R.id.apiLevel_TextView);
    apiLevelTextView.setText("Android API LEVEL: "+ apiLevel);

    cheatButton = (Button)findViewById(R.id.cheat_Button);
    cheatButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(QuizActivity.this, CheatActivity.class);
        i.putExtra(CheatActivity.KEY_EXTRA, answer);
        startActivityForResult(i, 0);
      }
    });

    trueButton = (Button)findViewById(R.id.true_Button);
    trueButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
       checkAnswer(true);
      }
    });

    falseButton = (Button)findViewById(R.id.false_Button);
    falseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        checkAnswer(false);
      }
    });

    nextButton = (Button)findViewById(R.id.next_Button);
    nextButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // increment
        if ( currIndex == questionBank.length-1 ) {
          currIndex = 0;
        } else {
          currIndex +=1;
        }
        setQuestionTextView(currIndex);
      }
    });

    previousButton = (Button)findViewById(R.id.previous_Button);
    previousButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        // decrement
        if ( currIndex == 0 ) {
          currIndex = questionBank.length-1;
        } else {
          currIndex -=1;
        }
        setQuestionTextView(currIndex);
      }
    });
  }

  private void setQuestionTextView(int currIndex){
    // get Qustion & Answer, then set text on TextView
    int questionRId = questionBank[currIndex].getQuestion();
    String question = getResources().getString(questionRId);
    answer = questionBank[currIndex].getAnswer();
    Log.d(TAG, "setQuestionTextView(): currIndex="+currIndex+ ",\t"+answer);
    questionTextView = (TextView)findViewById(R.id.question_TextView);
    questionTextView.setText("# "+currIndex + "\nQustion:  "+question+ "\n\nAnswer:  " +answer);
  }

  private void checkAnswer(boolean userAnswer) {
    answerTextView = (TextView) findViewById(R.id.answer_TextView);
    if ((answer == true && userAnswer == true) || (answer == false && userAnswer == false)) {
      answerTextView.setText("O - Correct!\nYou selected:    " +userAnswer+ "\nThe answer is:  " + answer);
      // when correct answer, advance to the next question
      if (currIndex == questionBank.length - 1) {
        currIndex = 0;
      } else {
        currIndex += 1;
      }
      setQuestionTextView(currIndex);
    } else {
      answerTextView.setText("X - Wrong!\nYou selected:    " +userAnswer+ "\nThe answer is:  " + answer);
    }
  }
}