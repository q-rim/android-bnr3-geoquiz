package com.q_rim.geoquiz;

public class TrueFalse {

  private int question;             // question is Android resource ID number.
  private boolean answer;           // this is manually set when creating this object.

  public TrueFalse(int question, boolean answer) {
    this.question = question;
    this.answer = answer;
  }

  public int getQuestion() {
    return question;
  }

  public void setQuestion(int question) {
    this.question = question;
  }

  public boolean getAnswer() {
    return answer;
  }

  public void setAnswer(boolean answer) {
    this.answer = answer;
  }
}
